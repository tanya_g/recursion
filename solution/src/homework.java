public class homework {

    public static void main(String args[]) {
        recursion(100);
    }


    public static int recursion(int i) {

        //stop when i<1. System counts from the top to the down
        if (i <1) {
            return i;
        } else if (i % 3 == 0 && i % 5 == 0) {
            System.out.println("FizzBuzz");
        } else if (i % 3 == 0) {
            System.out.println("Fizz");
        } else if (i % 5 == 0) {
            System.out.println("Buzz");
        } else
            System.out.println(i);
        return recursion(i -1);
    }
}
